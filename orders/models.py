import decimal

from django.conf import settings
from django.db import models
from django.urls import reverse


class Category(models.Model):
    name = models.CharField(max_length=32)
    only_one_price = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Topping(models.Model):
    name = models.CharField(max_length=32)
    price = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=32)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    slug = models.SlugField()
    small_price = models.DecimalField(max_digits=5, decimal_places=2)
    large_price = models.DecimalField(
        null=True, blank=True, max_digits=5, decimal_places=2
    )

    def __str__(self):
        return self.name


class Cart(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    start_date = models.DateTimeField(auto_now_add=True)
    ordered_date = models.DateTimeField(null=True)
    ordered = models.BooleanField(default=False)
    value = models.DecimalField(max_digits=5, decimal_places=2, default=0)

    def __str__(self):
        return f"{self.user.username} cart"


class OrderItem(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE, null=True, blank=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveSmallIntegerField(default=1)
    size = models.CharField(
        max_length=16, choices=(("S", "Small"), ("L", "Large")), default="S"
    )
    toppings = models.ManyToManyField(Topping, blank=True)

    @property
    def value(self):
        return self.get_single_product_value() * self.quantity

    def get_single_product_value(self):
        if self.size == "S":
            value = self.product.small_price
        else:
            value = self.product.large_price
        if self.toppings:
            for topping in self.toppings.all():
                value = decimal.Decimal(value) + topping.price
        return value

    def __str__(self):
        return f"{self.quantity} {self.size} {self.product}"
