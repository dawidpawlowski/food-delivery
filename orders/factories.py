import factory
import factory.fuzzy
from django.contrib.auth.models import User

from orders.models import *


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.fuzzy.FuzzyText(length=10)
    password = factory.fuzzy.FuzzyText(length=12)


class ToppingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Topping
        django_get_or_create = ("name", "price")

    name = factory.fuzzy.FuzzyText(length=10)
    price = factory.fuzzy.FuzzyFloat(low=0.5, high=3)


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Category
        django_get_or_create = ("name", "only_one_price")

    name = factory.fuzzy.FuzzyText(length=20)
    only_one_price = False


class ProductFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Product
        # django_get_or_create = ("name", "small_price", "large_price", "slug", "category")

    name = factory.fuzzy.FuzzyText(length=10)
    small_price = factory.fuzzy.FuzzyFloat(low=1, high=20)
    large_price = factory.fuzzy.FuzzyFloat(low=1, high=20)
    slug = factory.fuzzy.FuzzyText(length=6)
    category = factory.SubFactory(CategoryFactory)


class CartFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Cart
        django_get_or_create = (
            "user",
            "value",
        )

    value = factory.fuzzy.FuzzyFloat(low=1, high=100)
    user = factory.SubFactory(UserFactory)


class OrderItemFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = OrderItem
        # django_get_or_create = ("item", "quantity", "size", "topping")

    cart = factory.SubFactory(CartFactory)
    product = factory.SubFactory(ProductFactory)
    quantity = factory.fuzzy.FuzzyInteger(low=1, high=10)
    size = factory.Iterator(["Small", "Large"])
    toppings = factory.SubFactory(ToppingFactory)

    @factory.post_generation
    def toppings(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for topping in extracted:
                self.toppings.add(topping)
