from django.urls import path

from .views import (
    MenuView,
    add_to_cart,
    cart,
    decrease_quantity,
    increase_quantity,
    index,
    product_detail,
    remove_from_cart,
)

app_name = "orders"

urlpatterns = [
    path("", index, name="home"),
    path("menu", MenuView.as_view(), name="menu"),
    path("add-to-cart/", add_to_cart, name="add-to-cart"),
    path("remove-from-cart/<order_item_id>", remove_from_cart, name="remove-from-cart"),
    path("cart", cart, name="cart"),
    path("details/<str:slug>", product_detail, name="product_detial"),
    path("decrease/<order_item_id>", decrease_quantity, name="decrease-quantity"),
    path("increase/<order_item_id>", increase_quantity, name="increase-quantity"),
]
