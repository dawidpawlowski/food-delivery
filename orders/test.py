from unittest.mock import Mock, patch

from django.contrib.auth import login
from django.test import Client, TestCase

from orders.factories import *
from orders.forms import ProductDetailsForm

STATUS_OK = 200


class OrdersTest(TestCase):
    def setUp(self) -> None:
        self.user = UserFactory()
        self.topping = ToppingFactory()
        self.product = ProductFactory(name="test")
        self.order_item = OrderItemFactory()
        self.cart = CartFactory(user=self.user)

    def test_index_page_returns_status_code_ok(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, STATUS_OK)

    def test_menu_page_returns_menu(self):
        response = self.client.get("/menu")
        self.assertContains(response, "test")

    def test_user_can_add_to_cart(self):
        c = Client()
        c.force_login(user=self.user)
        data = {
            "user": self.user,
            "product": self.product,
            "size": "L",
            "quantity": 1,
            "topping": self.topping,
        }
        response = c.post("add-to-cart/", data)
        self.assertTrue(200, response.status_code)

    def test_product_display_in_cart(self):
        c = Client()
        c.force_login(user=self.user)
        data = {
            "user": self.user,
            "product": self.product,
            "size": "L",
            "quantity": 1,
            "topping": self.topping,
        }
        c.post("add-to-cart/", data)
        response = c.get("/cart")
        self.assertContains(response, self.user.username)

    def test_item_details_returns_valid_form(self):
        response = self.client.get("/details/" + self.product.slug)
        self.assertEqual(response.status_code, STATUS_OK)
        self.assertContains(response, self.topping)
        self.assertContains(response, "Quantity")
        self.assertContains(response, "Size")

    def test_small_size_order_item_value(self):
        product = ProductFactory(small_price=2)
        order_item = OrderItemFactory(
            product=product, quantity=1, size="S", toppings=[]
        )
        self.assertEqual(order_item.value, 2)

    def test_multi_large_size_order_item_value_with_toppings(self):
        product = ProductFactory(large_price=3.5)
        topping = ToppingFactory(name="cheese", price=1.2)
        toppin2 = ToppingFactory(name="peperoni", price=2)
        order_item = OrderItemFactory(
            product=product, quantity=2, size="L", toppings=[topping, toppin2]
        )
        self.assertEqual(float(order_item.value), 13.4)

    def test_add_to_cart(self):
        c = Client()
        c.force_login(user=self.user)
        data = {
            "user": self.user,
            "product": self.product,
            "size": "L",
            "quantity": 1,
            "topping": self.topping,
        }
        response = c.post("/details/" + self.product.slug, data, follow=True)
        self.assertEqual(response.redirect_chain[-1], ("/menu", 302))

    def test_increase_quantity(self):
        c = Client()
        c.force_login(user=self.user)
        quantity_before = self.order_item.quantity
        response = c.post("/increase/" + str(self.order_item.id))
        quantity_after = OrderItem.objects.get(id=self.order_item.id).quantity
        self.assertEqual(quantity_after, quantity_before + 1)

    def test_decrease_quantity(self):
        c = Client()
        c.force_login(user=self.user)
        quantity_before = self.order_item.quantity
        response = c.post("/decrease/" + str(self.order_item.id))
        # TODO: figure out why using self.order_item.quantity doesn't work
        # It looks like save() method in orders/views.py dont work on this field
        quantity_after = OrderItem.objects.get(id=self.order_item.id).quantity
        self.assertEqual(quantity_after, quantity_before - 1)

    def test_decrease_quantity_deletes_order_item_if_decrease_on_last_item(self):
        c = Client()
        c.force_login(user=self.user)
        self.order_item.quantity = 1
        self.order_item.save()
        count_objects_before = OrderItem.objects.filter(id=self.order_item.id).count()
        response = c.post("/decrease/" + str(self.order_item.id))
        count_objects_after = OrderItem.objects.filter(id=self.order_item.id).count()
        self.assertNotEqual(count_objects_after, count_objects_before)


    def test_remove_from_cart(self):
        c = Client()
        c.force_login(user=self.user)
        count_objects_before = OrderItem.objects.filter(id=self.order_item.id).count()
        response = c.post("/remove-from-cart/"+str(self.order_item.id))
        count_objects_after = OrderItem.objects.filter(id=self.order_item.id).count()
        self.assertNotEqual(count_objects_before, count_objects_after)
        self.assertEqual(count_objects_after, 0)

