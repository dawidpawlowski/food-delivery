from django import forms

from .models import OrderItem


class ProductDetailsForm(forms.ModelForm):
    class Meta:
        model = OrderItem
        fields = ["size", "quantity", "toppings"]
