from django.contrib import messages
from django.db.models import Count
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone
from django.views.generic import ListView

from .forms import ProductDetailsForm
from .models import *


def index(request):
    return render(request, "orders/index.html")


# Class view
class MenuView(ListView):
    model = Product
    template_name = "orders/menu.html"


def cart(request):
    context = {"cart": Cart.objects.get(user=request.user, ordered=False)}
    return render(request, "orders/cart.html", context)


def product_detail(request, slug):
    form = ProductDetailsForm(request.POST or None)
    product = Product.objects.get(slug=slug)
    product_information = {"user": request.user, "product": product}
    if form.is_valid():
        product_information.update(form.cleaned_data)
        return add_to_cart(request, data=product_information)
    context = {"form": form, "product": product}
    return render(request, "orders/productDetails.html", context)


def add_to_cart(request, data):
    user = data.pop("user")
    cart, created = Cart.objects.get_or_create(user=user, ordered=False)
    order_item = get_or_create_order_item(data)
    cart.value += order_item.value
    cart.save()

    if created or order_item not in cart.orderitem_set.all():
        order_item.cart = cart
        order_item.save()
    return redirect("orders:menu")


def get_or_create_order_item(data):
    toppings = data.pop("toppings")
    order_item = (
        OrderItem.objects.filter(
            product=data["product"], size=data["size"], toppings__in=toppings,
        )
        .annotate(num_topping=Count("toppings"))
        .filter(num_topping=len(toppings))
        .first()
    )
    if order_item:
        order_item.quantity += data["quantity"]
        order_item.save()

    if not order_item:
        order_item = OrderItem(
            product=data["product"], size=data["size"], quantity=data["quantity"],
        )
        order_item.save()
        for topping in toppings:
            order_item.toppings.add(topping)
            order_item.save()
    return order_item


def remove_from_cart(request, order_item_id):
    order_item = OrderItem.objects.get(id=order_item_id)
    cart = order_item.cart
    cart.value -= order_item.value
    order_item.delete()
    cart.save()
    return redirect("orders:cart")


def decrease_quantity(request, order_item_id):
    order_item = OrderItem.objects.get(id=order_item_id)
    cart = order_item.cart
    cart.value -= order_item.get_single_product_value()
    if order_item.quantity > 1:
        order_item.quantity -= 1
    else:
        order_item.delete()
    order_item.save()
    cart.save()
    return redirect("orders:cart")


def increase_quantity(request, order_item_id):
    order_item = OrderItem.objects.get(id=order_item_id)
    cart = order_item.cart
    order_item.quantity += 1
    cart.value += order_item.get_single_product_value()
    order_item.save()
    cart.save()
    return redirect("orders:cart")
